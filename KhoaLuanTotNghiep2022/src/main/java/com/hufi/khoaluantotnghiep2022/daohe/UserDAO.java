package com.hufi.khoaluantotnghiep2022.daohe;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.zkoss.zkplus.hibernate.HibernateUtil;

import com.hufi.khoaluantotnghiep2022.dao.GenericHibernateDAO;
import com.hufi.khoaluantotnghiep2022.entity.Users;
import com.hufi.khoaluantotnghiep2022.utils.HibernateUtils;

public class UserDAO extends GenericHibernateDAO{

	public UserDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	public Users saveOrUpdate(Users user) {
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(user);
            transaction.commit();
            return user;
		} catch (Exception e) {
			LOGGER.error(e);
			if (transaction != null)
				transaction.rollback();
			return new Users();
		} finally {
			if (session != null)
				session.close();
		}
	}
//
	public Boolean delete(Long id) {
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String hql = "DELETE USERS WHERE USER_ID = :id";
	        Query query = session.createSQLQuery(hql);
	        query.setParameter("id", id);
	        query.executeUpdate();
            transaction.commit();
            return true;
		} catch (Exception e) {
			LOGGER.error(e);
			if (transaction != null)
				transaction.rollback();
			return false;
		} finally {
			if (session != null)
				session.close();
		}
	}

	public Users findById(Long id) {
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("Users.findByUserId");
            query.setParameter("userId", id);
            List<Users> result = query.list();
            transaction.commit();
            if (result.isEmpty()) {
                return null;
            } else {
                return result.get(0);
            }
		} catch (Exception e) {
			LOGGER.error(e);
			if (transaction != null)
				transaction.rollback();
			return new Users();
		} finally {
			if (session != null)
				session.close();
		}
	}

	public List<Users> findAll() {
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			List<Users> lstUsers = session.createQuery("Select us from Users us").list();
			transaction.commit();
			return lstUsers;
		} catch (Exception e) {
			LOGGER.error(e);
			if(transaction != null)
				transaction.rollback();
			return new ArrayList<Users>();
		} finally {
			if(session != null)
				session.close();
		}
	}
	public Users findUserByUserNameAndPassword(String username, String password) {
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			StringBuffer hsql = new StringBuffer();
			hsql.append("Select us from Users us where us.userName = :userName AND us.password = :password");
			Query query = session.createQuery(hsql.toString());
			query.setParameter("userName", username);
			query.setParameter("password", password);
			List<Users> lstuser = query.list();
			transaction.commit();
			if(lstuser!=null && !lstuser.isEmpty())
				return lstuser.get(0);
		} catch (Exception e) {
			LOGGER.error(e);
			if(transaction != null)
				transaction.rollback();
			return new Users();
		} finally {
			if(session != null)
				session.close();
		}
		return null;
	}
//	Transaction transaction = null;
//	Session session = null;
//	try {
//		session = sessionFactory.openSession();
//		transaction = session.beginTransaction();

//		transaction.commit();
//		return ....;
//	} catch (Exception e) {
//		LOGGER.error(e);
//		if(transaction != null)
//			transaction.rollback();
//		return null;
//	} finally {
//		if(session != null)
//			session.close();
//	}
}
