package com.hufi.khoaluantotnghiep2022.utils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class StringUtils {

	private static final int CAPTCHA_LENGTH = 5;
    private static final String ALPHABEL = "abcdefghijklmnopqrstuvwxyz1234567890";
    private static final int ZERO = 0;
    private static final char SYMBOL = '"';
    private static final char[] SPECIAL_CHARACTERS = {' ', '!', '"', '#', '$', '%',
        '*', '+', ',', ':', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
        '`', '|', '~', 'À', 'Á', 'Â', 'Ã', 'È', 'É', 'Ê', 'Ì', 'Í', 'Ò',
        'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â', 'ã', 'è', 'é', 'ê',
        'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý', 'Ă', 'ă', 'Đ', 'đ',
        'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ', 'ạ', 'Ả', 'ả', 'Ấ',
        'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ',
        'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế',
        'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị',
        'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ',
        'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ',
        'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự', 'Ỹ',
        'ỹ',};
    private static final char[] REPLACEMENTS = {'-', '\0', '\0', '\0', '\0', '\0',
        '\0', '_', '\0', '_', '\0', '\0', '\0', '\0', '\0', '\0', '_',
        '\0', '\0', '\0', '\0', '\0', 'A', 'A', 'A', 'A', 'E', 'E', 'E',
        'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a', 'a',
        'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y', 'A',
        'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A', 'a',
        'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
        'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E', 'e',
        'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'I',
        'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
        'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
        'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
        'U', 'u', 'Y', 'y',};

    /**
     * Xu ly cho ham isBlank
     *
     * @author thonglv
     * @param str
     * @return
     * @since 19/09/2022
     */
    public static boolean isBlank(String str) {
        return org.apache.commons.lang3.StringUtils.isBlank(str);
    }

    /**
     * Escape html characters
     *
     * @param string input string
     * @return null if input string is null, escaped string if input string is
     * not null
     */
    public static String escapeHtml(String string) {
        if (string == null) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();
        char[] carr = string.toCharArray();
        for (int i = 0; i < carr.length; ++i) {
            char c = carr[i];
            switch (c) {
                case '\'':
                    stringBuilder.append("&#39;");
                    break;
                case '"':
                    stringBuilder.append("&quot;");
                    break;
                case '<':
                    stringBuilder.append("&lt;");
                    break;
                case '>':
                    stringBuilder.append("&gt;");
                    break;
                case '&':
                    stringBuilder.append("&amp;");
                    break;
                default:
                    stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }

    public static String escHtml(String input) {
        if (input == null) {
            return null;
        }
        String result = input;
        List<String[]> codecs = new ArrayList<String[]>();
        codecs.add(new String[]{"'", "&#39;"});
        codecs.add(new String[]{"\"", "&quot;"});
        codecs.add(new String[]{"<", "&lt;"});
        codecs.add(new String[]{">", "&gt;"});
        codecs.add(new String[]{"&", "&amp;"});
        codecs.add(new String[]{"]", "&#93;"});
        codecs.add(new String[]{"[", "&#91"});
        codecs.add(new String[]{"{", "&#123;"});
        codecs.add(new String[]{"}", "&#125;"});
        for (int i = 0; i < codecs.size(); i++) {
            if (result.contains(codecs.get(i)[0])) {
                result = result.replace(codecs.get(i)[0], codecs.get(i)[1]);
            }
        }
        return result;
    }

    /**
     * escape special character in sql
     *
     * @param input input
     * @return String
     */
    public static String escapeSql(String input) {
        String result = input.trim().replace("/", "//").replace("_", "/_").replace("%", "/%");
        return result;
    }

    public static <T> String join(T[] array, String cement, boolean isSql) {
        StringBuilder builder = new StringBuilder();

        if (array == null || array.length == 0) {
            return null;
        }

        for (T t : array) {
            builder.append((isSql == true) ? "'" + t.toString() + "'" : t).append(cement);
        }

        builder.delete(builder.length() - cement.length(), builder.length());

        return builder.toString();
    }

    /**
     * Get like string in sql
     *
     * @param content
     * @return String content
     */
    public static String toLikeAndLowerCaseString(String content) {
        return "%" + StringUtils.escapeSql(content.toLowerCase().trim()) + "%";
    }

    public static String toLikeAndLowerCase(String content) {
        return StringUtils.escapeSql(content.toLowerCase().trim());
    }

    public static String toLikeString(String content) {
        return "%" + StringUtils.escapeSql(content.trim()) + "%";
    }

    public static boolean checkEmail(String email) {
        final String EMAIL_REGEX = ".+@.+\\.[a-z]+";
        return email.matches(EMAIL_REGEX);
    }

    public static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replaceAll("đ", "d");
    }

    public static String getRandomString() {
        StringBuilder sb = new StringBuilder(CAPTCHA_LENGTH);
        Random rn = new Random();
        for (int i = 0; i < CAPTCHA_LENGTH; i++) {
            sb.append(ALPHABEL.charAt(rn.nextInt(ALPHABEL.length())));
        }
        return sb.toString();
    }

    public static String setSearchText(String inputStr) {
        if (inputStr == null || inputStr.trim().length() == 0) {
            return inputStr;
        }

        if (inputStr.charAt(ZERO) == SYMBOL && inputStr.endsWith("\"")) {
            return inputStr.substring(1, inputStr.length() - 1);
        } else {
            return inputStr;
        }
    }

    public static boolean isSearchUnsign(String inputStr) {
        if (inputStr == null || inputStr.trim().length() == 0) {
            return false;
        }
        return ((inputStr.charAt(ZERO) == SYMBOL) && inputStr.endsWith("\""));
    }

    public static String configColumnToSearchUnsign(String columnName, boolean isSearchUnsign) {
        return isSearchUnsign ? " CONVERT_TO_UNSIGN(lower(" + columnName + ")) " : " lower(" + columnName + ") ";
    }

    public static String toNoneUnicodeString(String s) {
        int maxLength = Math.min(s.length(), 236);
        char[] buffer = new char[maxLength];
        int n = 0;
        for (int i = 0; i < maxLength; i++) {
            Character ch = s.charAt(i);
            if (!ch.equals(' ')) {
                buffer[n] = removeAccent(ch);
            } else {
                buffer[n] = ' ';
            }

            if (buffer[n] > 31) {
                n++;
            }
        }
        // skip trailing slashes
        while (n > 0 && buffer[n - 1] == '/') {
            n--;
        }
        return String.valueOf(buffer, 0, n);
    }

    public static char removeAccent(char ch) {
        int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
        if (index >= 0) {
            ch = REPLACEMENTS[index];
        }
        return ch;
    }

    public static String truncate(final String content, final int lastIndex) {
        if (content == null || content.isEmpty()) {
            return content;
        }
        if (content.length() <= lastIndex) {
            return content;
        }
        String result = content.substring(0, lastIndex);
        if (content.charAt(lastIndex) != ' ') {
            int lIdx = result.lastIndexOf(" ");
            if (lIdx > 0) {
                result = result.substring(0, lIdx);
            }
        }
        return result + "...";
    }
}
