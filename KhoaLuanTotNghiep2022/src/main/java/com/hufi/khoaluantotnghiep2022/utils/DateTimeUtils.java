package com.hufi.khoaluantotnghiep2022.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {
    public static Date addMonths(Date date, int months) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
            c.add(Calendar.MONTH, months);
        }
        return c.getTime();
    }

    public static Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
            c.add(Calendar.DAY_OF_MONTH, days);
        }
        return c.getTime();
    }

    public static Date addYear(Date input, int year) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(input);
        cal.add(Calendar.YEAR, year);
        return cal.getTime();
    }

    public static Date addMinutes(Date date, int minutes) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
            c.add(Calendar.MINUTE, minutes);
        }
        return c.getTime();
    }

    public static java.util.Date convertStringToDate(String date, String pattern) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.parse(date);
    }

    public static java.util.Date convertStringToDate(String date) throws ParseException {
        if ((date == null) || (date.length() == 0)) {
            return null;
        }
        String pattern = "dd/MM/yyyy";
        return convertStringToDate(date, pattern);
    }

    public static String convertDateToString(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    public static String convertDateTimeToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
        if (date == null) {
            return "";
        }
        return dateFormat.format(date);
    }

    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        if (date == null) {
            return "";
        }
        return dateFormat.format(date);
    }

    public static String convertDateTimeToStringV2(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        if (date == null) {
            return "";
        }
        return dateFormat.format(date);
    }

    public static java.util.Date getDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Date getWeekStart(Date curr) {
        if (curr == null) {
            curr = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(curr);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);

        Date weekStart = c.getTime();

        return weekStart;
    }

    public static int getNumberOfWorkingDay(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);
        int startDay = startCal.get(Calendar.DAY_OF_WEEK);
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);
        endCal.set(Calendar.HOUR_OF_DAY, 23);
        endCal.set(Calendar.MINUTE, 59);
        endCal.set(Calendar.SECOND, 59);
        endCal.set(Calendar.MILLISECOND, 0);

        double daysBetween = Math.ceil(((double) (endCal.getTimeInMillis() - startCal.getTimeInMillis())) / (24 * 60 * 60 * 1000));
        int weeks = (int) Math.floor(daysBetween / 7) * 5;
        int remainDays = (int) Math.ceil(daysBetween % 7);
        int numberOfWorkingDays = weeks + remainDays;

        while ((Calendar.SUNDAY == startDay || Calendar.SATURDAY == startDay) && remainDays > 0) {
            if (Calendar.SATURDAY == startDay) {
                startDay = Calendar.SUNDAY;
                remainDays--;
                numberOfWorkingDays--;
                continue;
            }
            if (Calendar.SUNDAY == startDay) {
                startDay = Calendar.MONDAY;
                remainDays--;
                numberOfWorkingDays--;
            }
        }

        // Vì tính cả startDay nên số ngày thêm vào bị trừ đi 1
        // Ví dụ 16, 17 là 2 ngày. nên sẽ là 16 + 1 để ra ngày 17 (endDay) chứ
        // không phải là 16 + 2
        int offset = startDay + remainDays - 1 - Calendar.SATURDAY;
        if (offset == 0) {
            numberOfWorkingDays -= 1;
        } else if (offset >= 1) {
            numberOfWorkingDays -= 2;
        }
        return numberOfWorkingDays;
    }

    public static Date getEndWorkingDate(Date startDate, int numberOfWorkingDays) {
        int numberOfWorkingDayToAdd = numberOfWorkingDays;
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);
        int startDay = startCal.get(Calendar.DAY_OF_WEEK);

        while (Calendar.SATURDAY == startDay || Calendar.SUNDAY == startDay) {
            startCal.add(Calendar.DAY_OF_YEAR, 1);
            startDay = startCal.get(Calendar.DAY_OF_WEEK);
        }
        /*
		 * vì startDay khác thứ 7, CN nên số ngày được + sẽ giảm đi 1 do
		 * workingDay đã tính cả ngày hiện tại.
         */
        numberOfWorkingDayToAdd--;
        int numberOfWeekToAdd = numberOfWorkingDayToAdd / 5;
        int remainDays = numberOfWorkingDayToAdd % 5;

        while (remainDays > 0) {
            startCal.add(Calendar.DAY_OF_YEAR, 1);
            startDay = startCal.get(Calendar.DAY_OF_WEEK);
            while (Calendar.SATURDAY == startDay || Calendar.SUNDAY == startDay) {
                startCal.add(Calendar.DAY_OF_YEAR, 1);
                startDay = startCal.get(Calendar.DAY_OF_WEEK);
            }
            remainDays--;
        }

        if (numberOfWeekToAdd > 0) {
            startCal.add(Calendar.DAY_OF_YEAR, numberOfWeekToAdd * 7);
        }
        int endDay = startCal.get(Calendar.DAY_OF_WEEK);

        // Nếu endDate trùng với SUNDAY || SATURDAY thì lùi về FRIDDAY
        while (Calendar.SUNDAY == endDay || Calendar.SATURDAY == endDay) {
            startCal.add(Calendar.DAY_OF_YEAR, -1);
            endDay = startCal.get(Calendar.DAY_OF_WEEK);
        }
        return startCal.getTime();
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date setStartTimeOfDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    public static Date setEndTimeOfDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * phuctm2: trunc time of date
     *
     * @param date
     * @return
     */
    public static Date truncDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static boolean isLeapYear(int year) {
        return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
    }

    public static long getUnixTimeStampFromNow() {
        long unixTime = System.currentTimeMillis() / 1000L;
        return unixTime;
    }

    public static boolean after(Date source, Date destination) {
        if (source != null && destination != null) {
            return source.after(destination);
        }
        return false;
    }

    public static boolean before(Date source, Date destination) {
        if (source != null && destination != null) {
            return source.before(destination);
        }
        return false;
    }

    public static Date getFirstDayOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    public static Date getLastDayOfYear(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }
    //Thonglv
    public static Date getFirstDayOfYear_HH(int year) {
        Calendar calendar = Calendar.getInstance();
        Date timeNow = calendar.getTime();
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        if(timeNow.getMonth()>=9 && timeNow.getMonth()<=11) {
        	calendar.set(Calendar.YEAR, year);
        }
        else 
        	calendar.set(Calendar.YEAR, year-1);
        return calendar.getTime();
    }
    //hung hau
    public static Date getLastDayOfYear_HH(int year) {
        Calendar calendar = Calendar.getInstance();
        Date timeNow = calendar.getTime();
        calendar.set(Calendar.MONTH, 8);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        if(timeNow.getMonth()>=9 && timeNow.getMonth()<=11) {
        	calendar.set(Calendar.YEAR, year+1);
        }
        else 
        	calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }
    public static String getTotalTime(long totalTime) {
        long hours = totalTime / 60;
        long minus = totalTime - (hours * 60);
        if (hours > 0) {
            if (minus > 0) {
                return hours + " giờ " + minus + " phút";
            } else {
                return hours + " giờ";
            }
        } else {
            return minus + " phút";
        }
    }

    public static String convertDateTimeToString1(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        if (date == null) {
            return "";
        }
        return dateFormat.format(date);
    }
}
