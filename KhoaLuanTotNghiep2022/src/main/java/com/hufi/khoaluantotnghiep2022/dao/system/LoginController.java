package com.hufi.khoaluantotnghiep2022.dao.system;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Captcha;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.hufi.khoaluantotnghiep2022.daohe.UserDAO;
import com.hufi.khoaluantotnghiep2022.entity.Users;
import com.hufi.khoaluantotnghiep2022.utils.StringUtils;

public class LoginController extends SelectorComposer<Component>{

	private static final long serialVersionUID = 1884245004998867701L;
	private final String FOREGROUND_COLOR = "#000000";
	private final String BACKGROUND_COLOR = "#FDC966";
	@Wire
	Textbox txtCaptcha;
    @Wire
    private Captcha captcha;
    @Wire
    private Listbox lbxUsers;
//	@Wire
//	private Colorbox bgColorboxCaptcha;
//	@Wire
//	private Colorbox fgColorboxCaptcha;
	@Wire
	private Textbox txtUsername, txtPassword;
	@Wire
	private Button btnLogin;
	@Wire Window loginForm;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		if(captcha!=null) 
		{
			generateCaptchaContent();
		}	
		loadData();
		
	}
	private void generateCaptchaContent() {
		captcha.setWidth("150px");
		captcha.setHeight("35px");
        captcha.setValue(StringUtils.getRandomString());
        captcha.setFontColor(FOREGROUND_COLOR);
        captcha.setBgColor(BACKGROUND_COLOR);

//        bgColorboxCaptcha.setColor(BACKGROUND_COLOR);
//        fgColorboxCaptcha.setColor(FOREGROUND_COLOR);
    }

	private void loadData()
	{
		UserDAO usDao = new UserDAO();
		ListModelList<Users> lstUsers = new ListModelList<Users>(usDao.findAll());
		lbxUsers.setModel(lstUsers);
	}
	@Listen("onClick = #btnLogin; onOK=#loginForm")
	public void onLogin()
	{
		if(txtUsername==null || txtUsername.getValue()==null || txtUsername.getValue().toString().isEmpty())
		{
			Clients.showNotification("Tài khoản không được để trống.", "error", null, "middle_center", 2000);
			txtUsername.focus();
			return;
		}
		if(txtPassword==null || txtPassword.getValue()==null || txtPassword.getValue().toString().isEmpty())
		{
			Clients.showNotification("Vui lòng nhập mật khẩu.", "error", null, "middle_center", 2000);
			txtPassword.focus();
			return;
		}
//		if(captcha == null || captcha.getValue() == null)
//		{
//			Clients.showNotification("Xảy ra lỗi khi lấy mã captcha vui lòng làm mới lại trang.", "error", null, "middle_center", 2000);
//			return;
//		}
//		if(txtCaptcha == null || txtCaptcha.getValue()==null || !txtCaptcha.getValue().toString().equals(captcha.getValue().toString()))
//		{
//			Clients.showNotification("Mã xác nhận không hợp lệ.", "error", null, "middle_center", 2000);
//			txtCaptcha.focus();
//			generateCaptchaContent();
//			return;
//		}
		btnLogin.setDisabled(!btnLogin.isDisabled());
		UserDAO usDao = new UserDAO();
		Users userLogin = usDao.findUserByUserNameAndPassword(txtUsername.getValue().toString(),txtPassword.getValue().toLowerCase());
		if(userLogin == null)
		{
			Clients.showNotification("Tài khoản hoặc mật khẩu không chính xác", "error", null, "middle_center", 2000);
			btnLogin.setDisabled(!btnLogin.isDisabled());
			return;
		}
		Clients.showNotification("Đăng nhập thành công.", "info", null, "middle_center", 2000);
		btnLogin.setDisabled(!btnLogin.isDisabled());
	}
}
