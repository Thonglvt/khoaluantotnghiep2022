package com.hufi.khoaluantotnghiep2022.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USERS")
@XmlRootElement
@SequenceGenerator(name = "USERS_SEQ", sequenceName = "USERS_SEQ", allocationSize = 1)
@NamedQueries({
	 @NamedQuery(name = "Users.findByUserId", query = "SELECT us FROM Users us WHERE us.userId = :userId")
    })
public class Users{
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name = "USER_SEQ", sequenceName = "USER_SEQ", allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "USER_ID")
	@Expose
	@SerializedName("userId")
	private Long userId;
	
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "USER_NAME")
	@Expose
	@SerializedName("userName")
	private String userName;
	
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "PASSWORD")
	@Expose
	@SerializedName("password")
	private String password;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
