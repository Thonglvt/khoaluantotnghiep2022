package com.hufi.khoaluantotnghiep2022.service.resource;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.zkoss.zk.ui.event.Events;

import com.hufi.khoaluantotnghiep2022.daohe.UserDAO;
import com.hufi.khoaluantotnghiep2022.entity.Users;
import com.hufi.khoaluantotnghiep2022.utils.JsonConverter;

/*URI:
http(s)://<domain>:(port)/<YourApplicationName>/<UrlPattern in web.xml>/<path>
http://localhost:8080/ProjectName/rest/users
*/
@Path("/users")
public class UserService {
//	private static Logger logger = Logger.getLogger(UserService.class);
//	
	public UserService() {
	}
	
	@GET
	@Path("/list")
	@Consumes("application/json;charset=utf-8")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Users> getAll() {
		List<Users> lstUser = new UserDAO().findAll();
        return lstUser;
    }
	@GET
	@Path("/str")
	@Consumes("application/json;charset=utf-8")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String getString() {
		return "Hello World \n";
    }
	@GET
    @Path("/findById/{id}")
	@Consumes("application/json;charset=utf-8")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Users get(@PathParam("id") Long id) {
		return new UserDAO().findById(id);
	}
	
	@POST
	@Path("/saveOrUpdate")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Users saveOrUpdate(Users user) {
		Users userSave = new UserDAO().saveOrUpdate(user); 
		return userSave;
	}

	@DELETE
	@Path("/deleteById/{id}")
//	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Boolean delete(@PathParam("id") Long id) {
		return new UserDAO().delete(id);
	}
//	
//	@POST
//	@Path("/list")
//	@Consumes("application/json;charset=utf-8")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getListUsers() throws Exception {
//		HashMap result = new HashMap();
//		List<Users> lstUsers = new UserDAO().findAll();
//		result.put("documents", lstUsers);
//		result.put("message", "Lấy danh sách user thành công.");
//		result.put("status", Response.Status.OK.getStatusCode());
//		return Response.status(Response.Status.OK).entity(JsonConverter.<HashMap>serialize(result)).build();
//	}
	
	
}
